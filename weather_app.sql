-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2016 at 06:06 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wa`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` text NOT NULL,
  `password` text NOT NULL,
  `last_update` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `password`, `last_update`) VALUES
(1, 'test', 'test', NULL),
(2, 'penis', 'penis', NULL),
(3, 'eric', 'penis', NULL),
(4, 'dddddd', 'ddddddd', NULL),
(5, 'dddd', 'dddd', NULL),
(11, 'weather', '', NULL),
(12, 'weee', '', NULL),
(13, '', '', NULL),
(14, 'weather', 'weather', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `weather_locations`
--

CREATE TABLE IF NOT EXISTS `weather_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_f` int(11) NOT NULL COMMENT 'foreign',
  `lat` text NOT NULL,
  `lon` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `weather_locations`
--

INSERT INTO `weather_locations` (`id`, `user_f`, `lat`, `lon`) VALUES
(19, 1, '33.04', '-116.87'),
(25, 11, '40.05', '-82.54'),
(27, 11, '38.21', '-84.25'),
(28, 11, '42.83', '-74.08'),
(29, 11, '38.21', '-84.25'),
(30, 11, '38.21', '-84.25');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
