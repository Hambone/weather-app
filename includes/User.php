<?php

//includes
require_once('Connect.php');


//class to handle user creation, authentication
class User
{
    private $database;

    public function __construct()
    {
        $this->database = new Connect();
    }


    //check user is in the database with a password match, not the best way; quick and dirty
    function isUser($username, $password)
    {
        $query = "SELECT user_name, password FROM users WHERE user_name='" . $username ."' " . "and password='" . $password . "'";
        $result = $this->database->query($query);

        $rowCount = 0;
        while ($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            $rowCount++;
        }

        if ($rowCount == 0)
            return false;
        else
            return true;
    }

    //function; create a new user
    function addUser($username, $password)
    {
        //first we need to check to make user this user name has not been taken, if it has return false
        $query = "SELECT user_name FROM users WHERE user_name='" . $username ."'";
        $result = $this->database->query($query);

        $rowCount = 0;
        while ($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            $rowCount++;
        }

        //user name is already in db
        if ($rowCount >= 1)
        {
            return false;
        }

        //create the new user
        $query = "INSERT into users (id,user_name,password,last_update) VALUES (null," . "'" . $username . "','" . $password . "', null)";
        $this->database->query($query);

        return true;
    }


}