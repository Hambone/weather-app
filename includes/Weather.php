<?php

require_once('Connect.php');


class Weather
{
    private $connection;
    private $userWeather;    //the array of lat and lon for the user in the db
    private $id;

    public function __construct()
    {
        $this->connection = new Connect();
    }

    //for now paris ky  eventually the users location
    public function getMainWeather()
    {

        $w = $this->getWeatherZip("40361");

        if ($w)
        {
            $toReturn = array();
            $toReturn[] = $w->name;
            $toReturn[] = $w->sys->country;
            $toReturn[] = $w->coord->lat;
            $toReturn[] = $w->coord->lon;
            $toReturn[] = $w->main->temp;
            $toReturn[] = $w->weather[0]->description;


            return $toReturn;
        }
        return false;
    }

    public function randomColor()
    {
        $r = rand(1,4);

        if ($r == 1)
            return "#f17e7f";
        else if ($r == 2)
            return "#CD4571";
        else if ($r == 3)
            return "#367DB4";
        else if ($r == 4)
            return "#807871";

    }


    //gets the weather in the weather database based on username and stores each lat and lon in array
    //to pull for later use
    //this method assumes the user has been authenticated
    public function getUserWeather()
    {

        $query = "SELECT id FROM users WHERE user_name='" . $_SESSION['username'] ."' ";

        $result = $this->connection->query($query);
        while ($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            $this->id = $row['id'];
            break;
        }

        $this->updateUserWeatherLocation();
    }

    private function updateUserWeatherLocation()
    {
        if ($this->userWeatherSize() > 0)
        {
            unset($this->userWeather);   //unset then reset
        }

        $query = "SELECT * FROM weather_locations WHERE user_f='" .  $this->id ."' ";
        $result = $this->connection->query($query);
        while ($row = $result->fetch(PDO::FETCH_ASSOC))
        {
            //iterate through them all and add to array
            $this->userWeather[] = $row['lat'];
            $this->userWeather[] = $row['lon'];
        }
    }

    //returns the size of the array after iterating over lat and lon
    public function userWeatherSize()
    {
        return count($this->userWeather);
    }

    //returns the next Lat and Lon then removes it from the list
    public function getNextWeather()
    {
        if ($this->userWeatherSize() > 0)
        {
            //$toReturn = array($this->userWeather[$this->userWeatherSize()-2],$this->userWeather[$this->userWeatherSize()-1]);

            //$w = $this->getWeatherZip("40361");   //returns json obj
            $w = $this->getWeatherLatLon($this->userWeather[$this->userWeatherSize()-2],$this->userWeather[$this->userWeatherSize()-1]);

            if ($w)
            {
                $toReturn = array();
                $toReturn[] = $w->name;
                $toReturn[] = $w->sys->country;
                $toReturn[] = $w->coord->lat;
                $toReturn[] = $w->coord->lon;
                $toReturn[] = $w->main->temp;
                $toReturn[] = $w->weather[0]->description;

                array_pop($this->userWeather);
                array_pop($this->userWeather);
                return $toReturn;

            }
        }
        return false;
    }

    //add/remove lat and lon to db
    //pass in zip code, pull the zip code information from openweather
    public function addLocationUser($zip)
    {
        $w = $this->getWeatherZip($zip);

        if ($w)
        {
            //now it to the database based on lat and lon
            $this->addLocation($w->coord->lat,$w->coord->lon);
        }
    }


    //add/remove lat and lon to db
    private function addLocation($lat, $lon)
    {
        $query = "INSERT into weather_locations (id,user_f,lat,lon) VALUES (null," . "'" . $this->id . "','" . $lat . "','" . $lon . "')";
        $this->connection->query($query);
        $this->updateUserWeatherLocation();
    }

    public function removeLocation($lat, $lon)
    {
        $query = "DELETE FROM weather_locations WHERE user_f='" . $this->id . "' and lat='" . $lat . "' and lon='" . $lon . "' LIMIT 1";
        $this->connection->query($query);
        $this->updateUserWeatherLocation();
    }


    //returns decoded json from openweather by zip
    private function getWeatherZip($zip)
    {
        $url = "api.openweathermap.org/data/2.5/weather?zip=".$zip."&APPID=6149fdec0e45cd9074a3ba0657cdda99";

        //curl grabbing the info
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
        curl_close($ch);

        $w = json_decode($data);

        if ($w->cod == "404")
            return false;

        return $w;
    }

    //returns decoded json from openweather by zip
    private function getWeatherLatLon($lat, $lon)
    {
        $url = "api.openweathermap.org/data/2.5/weather?lat=".$lat ."&lon=".$lon."&APPID=6149fdec0e45cd9074a3ba0657cdda99";

        //curl grabbing the info
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
        curl_close($ch);

        $w = json_decode($data);

        if ($w->cod == "404")
            return false;

        return $w;
    }

    public function getURLRemove($lat, $lon)
    {
        //return "fish";
        //return '<a href="ha.php?lat=apples&lon=fish">yoyo</a>';
        return '<a href="remove.php?lat='.$lat.'&lon='.$lon .'">Remove</a>';
    }
}


