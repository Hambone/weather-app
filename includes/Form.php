<?php
//class used for form validation; just something thrown together

class Form
{
    private $errors;  //array that will contain all errors, this will be used on render time

    //constructor will connect to the database
    public function __construct()
    {
        $this->errors = array();
    }

    //basic form validation for logging
    //checks for blank fields
    public function formLogin()
    {
        if ($_POST['username']=='' || $_POST['password']='')
        {
            $this->errors[] = "username/password is blank";
        }
    }

    public function formNewUser()
    {
        if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['repassword']))
        {
            if ($_POST['username']=='' || $_POST['password']='' || $_POST['repassword'] =='')
            {
                $this->errors[] = "No field can be blank!";
            }

            else if ($_POST['password'] != $_POST['repassword'])
            {
                $this->errors[] = "passwords didn't match!";
            }
        }
        else
        {
            $this->errors[] = "No field can be blank!";
        }

    }

    public function customAddError($e)
    {
        $this->errors[] = $e;
    }

    //returns the size of the error array
    function errorSize()
    {
        return count($this->errors);
    }

    function nextError()
    {
        //return the next error and remove
        return array_pop($this->errors);
    }
}



?>