<?php

require_once('User.php');

//class to handle sessions

class Session
{
    //constructor will connect to the database
    public function __construct()
    {

    }

    public function startSession()
    {
        session_start();
    }

    public function redirect($redirect)
    {
        header("location: " . $redirect);
    }


    public function login()
    {
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'])
        {
            return true;
        }
        else if (isset($_POST['username']) && isset($_POST['password']))
        {
            //attempt to login the user
            $user_control = new User();
            if ($user_control->isUser($_POST['username'], $_POST['password']))
            {
                $_SESSION['logged_in']=true;
                $_SESSION['username'] =  $_POST['username'];
                return true;
            }

        }
        return false;
    }

    public function logout()
    {
        //cookie deleter
        if (ini_get("session.use_cookies"))
        {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();  //destroy the session

        header("location: index.php");    //redirect to the login page
    }

}

