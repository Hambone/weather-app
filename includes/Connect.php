<?php

//class to handle most if not all database interactions
class Connect
{

    private $db_host = "localhost";
    private $db_name = "wa";
    private $db_user = "wa_test";
    private $db_pass = "test";
    private $db ="";

    //constructor will connect to the database
    public function __construct()
    {
        $this->db = new PDO("mysql:host=" . $this->db_host . ";dbname=" . $this->db_name, $this->db_user, $this->db_pass);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    //run query on the database, returns data if success
    public function query($query)
    {
        $result = $this->db->query($query);

        if ($result)
        {
            return $result;
        }
        else
        {
            return false;
        }
    }

    //disconnect from the database
    public function disconnect()
    {
        $this->db = null;
    }
}

