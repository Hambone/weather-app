<?php

require_once('../includes/Session.php');
require_once('../includes/Weather.php');

$session = new Session();
$session->startSession();

if (!$session->login())
{
    $session->redirect("index.php");
}
else
{
    //enter a new location for user
    if (isset($_POST['zip']))
    {
        $weather = new Weather();
        $weather->getUserWeather();

        $weather->addLocationUser($_POST['zip']);
        $_POST['zip'] = '';

        $session->redirect("main.php");
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Add new weather location</title>
    <link rel="stylesheet" type="text/css" href="style/add.css">
</head>
<body>

<div class="form-style">
    <form action="addnew.php" method="post">
        <p>Zip</p>
        <input type="text" name="zip" size="5" maxlength="5" />
        <input type="submit" value="submit"  />
    </form>
    <div class="a-style"><a href="index.php">Home</a></div>
</div>
<br />

<br />
<br />

</body>
</html>
