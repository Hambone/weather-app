<?php

require_once('../includes/Session.php');
require_once('../includes/Weather.php');

$session = new Session();
$session->startSession();
if (!$session->login())
{
    $session->redirect("index.php");
}
else
{
    $weather = new Weather();
    $weather->getUserWeather();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main</title>

    <link rel="stylesheet" type="text/css" href="style/widgetin.css">
</head>
<body>

<div class="logout inner-text">
    <a href="logout.php">Logout</a>
</div>

<?php while ($weather->userWeatherSize() > 0) {
$w = $weather->getNextWeather();  $color = $weather->randomColor(); ?>
<div class="widget bg-one">
    <div class="inner-header inner-text inner-header-text" style="background-color: <?php echo $color ?> !important">
        <span><?php echo $w[0] . ", " .$w[1]?></span>
        <div><?php echo "Lat: " . $w[2] . "  Lon: " .$w[3]?></div>
    </div>
    <div class="inner-mid-lg inner-text" style="background-color: <?php echo $color ?> !important">
        <div class= "inner-text-large-temp ">
            <?php echo (floor(($w[4]-273.15) * 1.8000 + 32.00))?> <sup>o F</sup>
        </div>
    </div>
    <div class="inner-mid-md inner-text">
        <div class="inner-mid-md-text">
            <?php echo $w[5]?>
        </div>
    </div>
    <div class="inner-footer inner-text ">
        <div class="inner-footer-text">
            <?php echo $weather->getURLRemove($w[2], $w[3]); ?>
        </div>
    </div>
</div>
<?php }?>


<a href="addnew.php"><div id="widget-new-border" class="widget widget-new">
        <div class="inner-header inner-text inner-header-text ">

        </div>

        <div class="inner-mid-lg inner-text widget-new">

        </div>

        <div class="inner-mid-md inner-text widget-new">
            <span id="special-text">+</span>
        </div>

        <div id="special-rad" class="inner-footer inner-text widget-new">

        </div>
    </div></a>

</body>
</html>

