<?php

require_once('../includes/Session.php');
require_once('../includes/Weather.php');

$session = new Session();
$session->startSession();
if (!$session->login())
{
    $session->redirect("index.php");
}
else
{
    if (isset($_GET['lat']) && isset($_GET['lon']))
    {
        $weather = new Weather();
        $weather->removeLocation($_GET['lat'],$_GET['lon']);
        $session->redirect("main.php");
    }
}
?>