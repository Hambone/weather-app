<?php

require_once('../includes/Session.php');

$session = new Session();
$session->startSession();
//required if statement
if ($session->login())
{
    $session->logout();
}

?>