<?php

require_once('../includes/Session.php');
require_once('../includes/Form.php');
require_once('../includes/Weather.php');


$form = new Form();
$session = new Session();
$session->startSession();

$weather = new Weather();

if ($session->login())
{
    $session->redirect("main.php");
}
else
{
    if (isset($_POST['submit']))
    {
        $form->formLogin();    //form login error checking
        $form->customAddError("username/password combination are invalid");
        unset($_POST['submit']);
    }


    while($form->errorSize() > 0)
    {
        echo $form->nextError();
        echo '<br />';
    }

    unset($_POST['username']);
    unset($_POST['password']);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Basic Widget Style</title>

    <link rel="stylesheet" type="text/css" href="style/widget.css">

</head>
<body>


<div class="login">
    <div class="form-style">
        <div class="form-style-detail">
            <div id="login" class="inner-text"> Login</div>
            <form action="main.php" method="post">
                <div class="input-margin"><input type="text" name="username" placeholder="username"></div>
                <div class="input-margin"><input type="password" name="password" placeholder="password" ></div>
                <div><input type="submit" value="submit" id="submit" name="submit" action="submit"></div>
            </form>
        </div>
    </div>
</div>

<?php $w = $weather->getMainWeather();
$color = $weather->randomColor();    ?>
    <div class="widget">
        <div class="inner-header inner-text inner-header-text" style="background-color: <?php echo $color ?> !important">
            <span><?php echo $w[0] . ", " .$w[1]?></span>
            <div><?php echo "Lat: " . $w[2] . "  Lon: " .$w[3]?></div>
        </div>
        <div class="inner-mid-lg inner-text" style="background-color: <?php echo $color ?> !important">
            <div class= "inner-text-large-temp">
                <?php echo (floor(($w[4]-273.15) * 1.8000 + 32.00))?> <sup>o F</sup>
            </div>
        </div>
        <div class="inner-mid-md inner-text">
            <div class="inner-mid-md-text">
                <?php echo $w[5]?>
            </div>
        </div>
        <div class="inner-footer inner-text ">
            <div class="inner-footer-text">
                Login to customize locations
            </div>
        </div>
    </div>





</body>
</html>



